package com.kami.galaxyescape;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class GalaxyEscape extends Game {
	public static final String ATLAS_INGAME = "ingame.pack";
	public static final String TEXTURE_BACKGROUND = "bg.png";

	private GameScreen gameScreen;
	private static AssetManager assetManager;

	public GalaxyEscape(){
		super();
	}

	@Override
	public void create () {
		assetManager = new AssetManager();

		// Load everything here
		assetManager.load(ATLAS_INGAME, TextureAtlas.class);
		assetManager.load(TEXTURE_BACKGROUND, Texture.class);

		this.gameScreen = new GameScreen(this);
		this.setScreen(gameScreen);
	}

	public static TextureAtlas getAtlas(String atlas){
		return assetManager.get(atlas, TextureAtlas.class);
	}

	public static Texture getTexture(String path){
		return assetManager.get(path, Texture.class);
	}
}
