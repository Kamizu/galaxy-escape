package com.kami.galaxyescape;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * @author Kami Nasri
 */
public class GameScreen implements Screen {
    public static final int CAMERA_WIDTH = 800;
    public static final int CAMERA_HEIGHT = 480;

    private SpriteBatch batch;
    private OrthographicCamera camera;

    // Background
    private Texture background;
    private int sourceY;

    public GameScreen(Game game){
        this.batch = new SpriteBatch();
        this.camera = new OrthographicCamera();
        this.camera.setToOrtho(false, CAMERA_WIDTH, CAMERA_HEIGHT);
        this.camera.update();

        background = new Texture("bg.png");
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));

        this.camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
            //Draw background
            sourceY += 1; //Scrolling speed for background
            background.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
            batch.draw(background, 0, 0, 0, sourceY, CAMERA_WIDTH, CAMERA_HEIGHT);

            
        batch.end();
    }

    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
